package br.com.zup.ecommercezup.controllers;

import br.com.zup.ecommercezup.dtos.CompraClienteDTO;
import br.com.zup.ecommercezup.dtos.CompraDTO;
import br.com.zup.ecommercezup.services.CompraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/compras")
public class CompraController {

    @Autowired
    private CompraService compraService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void cadastrarCompra(@RequestBody CompraDTO compraDTO){
        this.compraService.carregarDadosCliente(compraDTO);
        this.compraService.verificarEstoque(compraDTO);
        this.compraService.carregarDadosProduto(compraDTO);
        this.compraService.adicionarCompra(compraDTO);
    }

    @GetMapping
    public List<CompraDTO> exibirListaCompras(){
        return this.compraService.getListCompras();
    }

    @GetMapping("/cliente/{cpf}")
    public CompraClienteDTO pesquisarComprasPorCpf(@PathVariable String cpf){
        List<CompraDTO> comprasCliente;
        comprasCliente = this.compraService.pesquisaComprasPorCpf(cpf);
        return this.compraService.converterListaCompraDeCliente(comprasCliente);
    }

}
