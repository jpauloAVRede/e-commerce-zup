package br.com.zup.ecommercezup.controllers;

import br.com.zup.ecommercezup.dtos.ProdutoDTO;
import br.com.zup.ecommercezup.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void cadastrarProduto(@RequestBody @Valid ProdutoDTO produtoDTO){
        this.produtoService.adicionarProdutoNaLista(produtoDTO);
    }

    @GetMapping
    public List<ProdutoDTO> exibirListaProdutos(){
        return this.produtoService.getListaProdutos();
    }

}
