package br.com.zup.ecommercezup.controllers;

import br.com.zup.ecommercezup.dtos.ClienteDTO;
import br.com.zup.ecommercezup.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/clientes")
public class ClienteController {
    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void cadastrarCliente(@RequestBody @Valid ClienteDTO clienteDTO){
        this.clienteService.pesquisaDuplicidadeCpf(clienteDTO.getCpf());
        this.clienteService.pesquisaClienteEmail(clienteDTO.getEmail());
        this.clienteService.adicionarClienteNaLista(clienteDTO);
    }

    @GetMapping("/cliente/{cpf}")
    public ClienteDTO pesquisaClienteCpf(@PathVariable String cpf){
        return this.clienteService.pesquisaClienteCpf(cpf);
    }

}
