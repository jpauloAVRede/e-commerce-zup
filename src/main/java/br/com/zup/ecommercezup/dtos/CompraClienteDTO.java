package br.com.zup.ecommercezup.dtos;

import java.util.List;

public class CompraClienteDTO {
    private String nome;
    private List<ProdutoFiltradoCompraCliente> produtosCliente;

    public CompraClienteDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ProdutoFiltradoCompraCliente> getProdutosCliente() {
        return produtosCliente;
    }

    public void setProdutosCliente(List<ProdutoFiltradoCompraCliente> produtosCliente) {
        this.produtosCliente = produtosCliente;
    }
}
