package br.com.zup.ecommercezup.dtos;

public class ProdutoFiltradoCompraCliente {
    private String nome;
    private double preco;

    public ProdutoFiltradoCompraCliente() {
    }

    public ProdutoFiltradoCompraCliente(String nome, double preco) {
        this.nome = nome;
        this.preco = preco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
