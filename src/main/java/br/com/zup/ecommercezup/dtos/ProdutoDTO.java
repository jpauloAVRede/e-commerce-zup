package br.com.zup.ecommercezup.dtos;

import javax.validation.constraints.*;

public class ProdutoDTO {
    @Size(min = 2, max = 20, message = "{validacao.nome}")
    private String nome;
    @DecimalMin(value = "0.1", message = "{validacao.valorProduto}")
    private double preco;
    @Min(value = 0, message = "{validacao.qtdProduto}")
    private int quantidade;

    public ProdutoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public String toString() {
        return "ProdutoDTO{" +
                "nome='" + nome + '\'' +
                ", quantidade=" + quantidade +
                '}';
    }
}
