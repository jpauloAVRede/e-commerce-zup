package br.com.zup.ecommercezup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ECommerceZupApplication {

	public static void main(String[] args) {
		SpringApplication.run(ECommerceZupApplication.class, args);
	}

}
