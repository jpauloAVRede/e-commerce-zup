package br.com.zup.ecommercezup.services;

import br.com.zup.ecommercezup.dtos.ClienteDTO;
import br.com.zup.ecommercezup.exceptions.ClienteNaoCadastradoException;
import br.com.zup.ecommercezup.exceptions.CpfDuplicadoException;
import br.com.zup.ecommercezup.exceptions.EmailDuplicadoException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService {

    private List<ClienteDTO> listaClientes = new ArrayList<>();

    public List<ClienteDTO> getListaClientes() {
        return listaClientes;
    }

    public void adicionarClienteNaLista(ClienteDTO clienteDTO){
        this.listaClientes.add(clienteDTO);
    }

    public ClienteDTO pesquisaClienteCpf(String cpf){
        for (ClienteDTO cliente : this.listaClientes){
            if (cliente.getCpf().equals(cpf)){
                return cliente;
            }
        }
        throw new ClienteNaoCadastradoException("Cliente não cadastrado");
    }

    public void pesquisaDuplicidadeCpf(String cpf){
        for (ClienteDTO cliente : this.listaClientes){
            if (cliente.getCpf().equals(cpf)){
                throw new CpfDuplicadoException("CPF já em uso");
            }
        }
    }

    public void pesquisaClienteEmail(String email){
        for (ClienteDTO cliente : this.listaClientes) {
            if (cliente.getEmail().equals(email)) {
                throw new EmailDuplicadoException("Email já em uso");
            }
        }
    }

}
