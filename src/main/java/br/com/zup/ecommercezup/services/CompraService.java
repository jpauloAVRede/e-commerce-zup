package br.com.zup.ecommercezup.services;

import br.com.zup.ecommercezup.dtos.*;
import br.com.zup.ecommercezup.exceptions.PesqCompraPorCpfException;
import br.com.zup.ecommercezup.exceptions.VerificaEstoqueException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompraService {

    private List<CompraDTO> listCompras = new ArrayList<>();
    @Autowired
    private ClienteService clienteService;
    @Autowired
    private ProdutoService produtoService;

    public List<CompraDTO> getListCompras() {
        return this.listCompras;
    }

    public void adicionarCompra(CompraDTO compraDTO){
        this.listCompras.add(compraDTO);
    }

    public CompraDTO carregarDadosCliente(CompraDTO compraDTO){
        ClienteDTO clienteCarregado;

        clienteCarregado = this.clienteService.pesquisaClienteCpf(compraDTO.getCliente().getCpf());
        compraDTO.setCliente(clienteCarregado);

        return compraDTO;
    }

    public CompraDTO carregarDadosProduto(CompraDTO compraDTO){
        List<ProdutoDTO> listProdutoCarregado = new ArrayList<>();
        ProdutoDTO produtoCarregado;

        for (ProdutoDTO produto : compraDTO.getProdutos()){
            produtoCarregado = this.produtoService.pesquisaProdutoNome(produto.getNome());
            listProdutoCarregado.add(produtoCarregado);
        }
        compraDTO.setProdutos(listProdutoCarregado);

        return compraDTO;
    }

    public void verificarEstoque(CompraDTO compraDTO){
        List<String> listProdutoSemEstoque = new ArrayList<>();
        ProdutoDTO produtoEstoque;
        int i = 0;

        for (ProdutoDTO protudo : compraDTO.getProdutos()){
            produtoEstoque = this.produtoService.pesquisaProdutoNome(protudo.getNome());
            if (produtoEstoque.getQuantidade() == 0){
                listProdutoSemEstoque.add(produtoEstoque.getNome());
                i = 1;
            }
        }
        if (i == 1){
            throw new VerificaEstoqueException("Produto Sem Estoque", listProdutoSemEstoque);
        }
    }

    public List<CompraDTO> pesquisaComprasPorCpf(String cpf){
        List<CompraDTO> listaCompraClienteEspecifico = new ArrayList<>();
        int i = 0;

        for (CompraDTO compra : this.listCompras){
            if (compra.getCliente().getCpf().equals(cpf)){
                listaCompraClienteEspecifico.add(compra);
                i = 1;
            }
        }

        if (i == 1){
            return listaCompraClienteEspecifico;
        }
        throw new PesqCompraPorCpfException("Este cliente não possui compra conosco.");
    }

    public CompraClienteDTO converterListaCompraDeCliente(List<CompraDTO> comprasCliente){
        List<ProdutoFiltradoCompraCliente> listProduto = new ArrayList<>();
        CompraClienteDTO compraClienteDTO = new CompraClienteDTO();
        String nomeProvisorio = null;

        for (CompraDTO compra : comprasCliente){
            nomeProvisorio = compra.getCliente().getNome();
            for (ProdutoDTO produtoDTO : compra.getProdutos()){
                listProduto.add(new ProdutoFiltradoCompraCliente(produtoDTO.getNome(), produtoDTO.getPreco()));
            }
        }

        compraClienteDTO.setNome(nomeProvisorio);
        compraClienteDTO.setProdutosCliente(listProduto);

        return compraClienteDTO;
    }

}
