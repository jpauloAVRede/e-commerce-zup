package br.com.zup.ecommercezup.services;

import br.com.zup.ecommercezup.dtos.ProdutoDTO;
import br.com.zup.ecommercezup.exceptions.ProdutoDuplicadoException;
import br.com.zup.ecommercezup.exceptions.ProdutoNaoCadastradoException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProdutoService {
    private List<ProdutoDTO> listaProdutos = new ArrayList<>();

    public List<ProdutoDTO> getListaProdutos() {
        return listaProdutos;
    }

    public void adicionarProdutoNaLista(ProdutoDTO produtoDTO){
        verificaDuplicidadeProduto(produtoDTO.getNome());
        this.listaProdutos.add(produtoDTO);
    }

    public void verificaDuplicidadeProduto(String nomeProduto){
        for (ProdutoDTO produto : this.listaProdutos){
            if (produto.getNome().equals(nomeProduto)){
                throw new ProdutoDuplicadoException("Produto já cadastrado");
            }
        }
    }

    public ProdutoDTO pesquisaProdutoNome(String nomeProduto){
        for (ProdutoDTO produto : this.listaProdutos){
            if (produto.getNome().equals(nomeProduto)){
                return produto;
            }
        }
        throw new ProdutoNaoCadastradoException("Produto não cadastrado");
    }

}
