package br.com.zup.ecommercezup.exceptions;

public class ProdutoDuplicadoException extends RuntimeException{
    private String statusCode;

    public ProdutoDuplicadoException(String message) {
        super(message);
    }
}
