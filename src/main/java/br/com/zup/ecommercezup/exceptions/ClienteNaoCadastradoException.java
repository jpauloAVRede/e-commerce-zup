package br.com.zup.ecommercezup.exceptions;

public class ClienteNaoCadastradoException extends RuntimeException{
    private int statusCode = 400;

    public ClienteNaoCadastradoException(String message) {
        super(message);
    }
}
