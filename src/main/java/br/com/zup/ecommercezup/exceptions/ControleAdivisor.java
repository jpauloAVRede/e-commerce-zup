package br.com.zup.ecommercezup.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControleAdivisor {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro manipularExceccoesDeValidacao(MethodArgumentNotValidException exception) {
        List<FieldError> fieldErros = exception.getBindingResult().getFieldErrors();
        List<Erro> erros = fieldErros.stream().map(objeto -> new Erro(objeto.getDefaultMessage())).collect(Collectors.toList());

        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(ClienteNaoCadastradoException.class)
    public MensagemDeErro manipularClienteNaoCad(ClienteNaoCadastradoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(CpfDuplicadoException.class)
    public MensagemDeErro manipularCpfDuplicado(CpfDuplicadoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(EmailDuplicadoException.class)
    public MensagemDeErro manipularEmailDuplicado(EmailDuplicadoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(ProdutoDuplicadoException.class)
    public MensagemDeErro manipularProdutoDuplicado(ProdutoDuplicadoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(ProdutoNaoCadastradoException.class)
    public MensagemDeErro manipularProdutoNaoCadastrado(ProdutoNaoCadastradoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(VerificaEstoqueException.class)
    public MensagemDeErro manipularVerificaEstoque(VerificaEstoqueException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros, exception.getProdutosSemEstoque());
    }

    @ExceptionHandler(PesqCompraPorCpfException.class)
    public MensagemDeErro manipularPesqCompraPorCpf(PesqCompraPorCpfException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

}
