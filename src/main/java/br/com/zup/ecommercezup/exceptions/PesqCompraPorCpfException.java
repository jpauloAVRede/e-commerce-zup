package br.com.zup.ecommercezup.exceptions;

public class PesqCompraPorCpfException extends RuntimeException{
    private String statusCode;

    public PesqCompraPorCpfException(String message) {
        super(message);
    }
}
