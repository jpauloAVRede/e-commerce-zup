package br.com.zup.ecommercezup.exceptions;

import br.com.zup.ecommercezup.dtos.ProdutoDTO;

import java.util.List;

public class VerificaEstoqueException extends RuntimeException{
    private String statusCode;

    private List<String> produtosSemEstoque;

    public VerificaEstoqueException(String message, List<String> produtosSemEstoque) {
        super(message);
        this.produtosSemEstoque = produtosSemEstoque;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<String> getProdutosSemEstoque() {
        return produtosSemEstoque;
    }

    public void setProdutosSemEstoque(List<String> produtosSemEstoque) {
        this.produtosSemEstoque = produtosSemEstoque;
    }
}
