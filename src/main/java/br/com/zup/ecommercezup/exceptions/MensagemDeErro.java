package br.com.zup.ecommercezup.exceptions;

import java.util.List;

public class MensagemDeErro {
    private int statusCode;
    private List<Erro> erros;
    private List<String> produtosSemEstoque;

    public MensagemDeErro(int statusCode, List<Erro> erros, List<String> produtosSemEstoque) {
        this.statusCode = statusCode;
        this.erros = erros;
        this.produtosSemEstoque = produtosSemEstoque;
    }
    public MensagemDeErro(int statusCode, List<Erro> erros) {
        this.statusCode = statusCode;
        this.erros = erros;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<Erro> getErros() {
        return erros;
    }

    public void setErros(List<Erro> erros) {
        this.erros = erros;
    }

    public List<String> getProdutosSemEstoque() {
        return produtosSemEstoque;
    }

    public void setProdutosSemEstoque(List<String> produtosSemEstoque) {
        this.produtosSemEstoque = produtosSemEstoque;
    }
}
