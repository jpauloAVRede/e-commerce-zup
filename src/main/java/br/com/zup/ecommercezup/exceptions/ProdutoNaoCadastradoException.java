package br.com.zup.ecommercezup.exceptions;

public class ProdutoNaoCadastradoException extends RuntimeException{
    private String statusCode;

    public ProdutoNaoCadastradoException(String message) {
        super(message);
    }
}
