package br.com.zup.ecommercezup.exceptions;

public class CpfDuplicadoException extends RuntimeException{
    private String statusCode;

    public CpfDuplicadoException(String message) {
        super(message);
    }
}
