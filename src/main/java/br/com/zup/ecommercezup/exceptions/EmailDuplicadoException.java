package br.com.zup.ecommercezup.exceptions;

public class EmailDuplicadoException extends RuntimeException{
    private String statusCode;

    public EmailDuplicadoException(String message) {
        super(message);
    }
}
